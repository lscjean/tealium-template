//~~tv:20099.20180620
//~~tc: Add tealium_cookie_sync query string parameter

//tealium universal tag - utag.sender.20099 ut4.0.##UTVERSION##, Copyright ##UTYEAR## Tealium.com Inc. All Rights Reserved.
try {
  (function (id, loader) {
    var u = {"id" : id};
    utag.o[loader].sender[id] = u;
    // Please do not modify
    if (utag.ut === undefined) { utag.ut = {}; }
    // Start Tealium loader 4.35
    var match = /ut\d\.(\d*)\..*/.exec(utag.cfg.v);
    if (utag.ut.loader === undefined || !match || parseInt(match[1]) < 35) { u.loader = function (o) { var b, c, l, a = document; if (o.type === "iframe") { b = a.createElement("iframe"); o.attrs = o.attrs || { "height" : "1", "width" : "1", "style" : "display:none" }; for( l in utag.loader.GV(o.attrs) ){ b.setAttribute( l, o.attrs[l] ); } b.setAttribute("src", o.src); }else if (o.type=="img"){ utag.DB("Attach img: "+o.src); b=new Image();b.src=o.src; return; }else{ b = a.createElement("script");b.language="javascript";b.type="text/javascript";b.async=1;b.charset="utf-8"; for( l in utag.loader.GV(o.attrs) ){ b[l] = o.attrs[l]; } b.src = o.src; } if(o.id){b.id=o.id}; if (typeof o.cb=="function") { if(b.addEventListener) { b.addEventListener("load",function(){o.cb()},false); }else { /* old IE support */ b.onreadystatechange=function(){if(this.readyState=='complete'||this.readyState=='loaded'){this.onreadystatechange=null;o.cb()}}; } } l = o.loc || "head"; c = a.getElementsByTagName(l)[0]; if (c) { utag.DB("Attach to "+l+": "+o.src); if (l == "script") { c.parentNode.insertBefore(b, c); } else { c.appendChild(b) } } } } else { u.loader = utag.ut.loader; }
    // End Tealium loader
    // Start Tealium typeOf 4.35
    if (utag.ut.typeOf === undefined) { u.typeOf = function(e) {return ({}).toString.call(e).match(/\s([a-zA-Z]+)/)[1].toLowerCase();};} else { u.typeOf = utag.ut.typeOf; }
    // End Tealium typeOf

    u.ev = {"view" : 1};

    ##UTGEN##

    u.send = function (a, b) {
      if (u.ev[a] || u.ev.all !== undefined) {
        utag.DB("send:##UTID##");
        utag.DB(b);

        var c, d, e, f, g;

        u.data = {
          "qsp_delim" : "&",
          "kvp_delim" : "=",
          "base_url_req_ttdid" : "//match.adsrvr.org/track/cmf/generic?",
          "base_url_get_ttdid" : "//datacloud.tealiumiq.com/tealium_ttd/main/16/i.js?jsonp=utag.ut.tealium_pass_ttdid",
          "base_url_pass_ttdid" : "//datacloud.tealiumiq.com/vdata/i.gif?",
          "tealium_trace_id" : "",
          "tealium_account" : "##UTVARconfig_tealium_account##",
          "tealium_profile" : "##UTVARconfig_tealium_profile##",
          "ttd_pid" : "tealium",
          "ttd_tpi" : "1",
          "gdpr" : "0",
          "gdpr_consent" : "",
          "delay" : 5000,
          "interval" : 0,
          "sin" : 4
        };

        // Start tag-scoped extensions
        ##UTEXTEND##
        // End tag-scoped extensions

        utag.DB("send:##UTID##:EXTENSIONS");
        utag.DB(b);

        c = [];
        g = [];

        // Start Mapping
        for (d in utag.loader.GV(u.map)) {
          if (b[d] !== undefined && b[d] !== "") {
            e = u.map[d].split(",");
            for (f = 0; f < e.length; f++) {
              u.data[e[f]] = b[d];
            }
          }
        }
        utag.DB("send:##UTID##:MAPPINGS");
        utag.DB(u.data);
        // End Mapping

        // Set Tealium Account / Profile
        u.data.tealium_account = u.data.tealium_account || utag.cfg.utid.split("/")[0];
        u.data.tealium_profile = u.data.tealium_profile || utag.cfg.utid.split("/")[1];

        // Pull E-Commerce extension values
        // Mappings override E-Commerce extension values
        u.data.order_id = u.data.order_id || b._corder || "";

        if (!b["cp.utag_main_ttd_uuid"] && !u.data.order_id) {
          c.push("ttd_pid" + u.data.kvp_delim + u.data.ttd_pid);
          c.push("ttd_tpi" + u.data.kvp_delim + u.data.ttd_tpi);
          c.push("gdpr" + u.data.kvp_delim + u.data.gdpr);
          if (u.data.gdpr === "1" && u.typeOf(u.data.gdpr_consent) === "string" && u.data.gdpr_consent !== "") {
            c.push("gdpr_consent" + u.data.kvp_delim + u.data.gdpr_consent);
          }
          u.loader({
            "type" : "img",
            "src" : u.data.base_url_req_ttdid + c.join(u.data.qsp_delim)
          });

          utag.ut.tealium_pass_ttdid = function (o) {
            try {
              var tl = o.tvt ? o.tvt.length : 0;
              if (tl != 0) {
                utag.loader.SC("utag_main", {"ttd_uuid" : (o.tvt[tl - 1].t1 + ";exp-session")});
                g.push("ttd_uuid" + u.data.kvp_delim + o.tvt[tl - 1].t1);
                g.push("tealium_vid" + u.data.kvp_delim + b["cp.utag_main_v_id"]);
                g.push("tealium_account" + u.data.kvp_delim + u.data.tealium_account);
                g.push("tealium_profile" + u.data.kvp_delim + u.data.tealium_profile);
                g.push("tealium_cookie_sync" + u.data.kvp_delim + "true");
                if (u.data.tealium_trace_id) {
                    g.push("tealium_trace_id" + u.data.kvp_delim + u.data.tealium_trace_id);
                }
                u.loader({
                  "type" : "img",
                  "src" : u.data.base_url_pass_ttdid + g.join(u.data.qsp_delim)
                });
                clearInterval(u.polling_interval);
              }
            } catch (e) {
              utag.DB(e);
            }
          }

          u.get_ttdid = function (interval, sin) {
            if (interval >= sin) {
              clearInterval(u.polling_interval);
            } else {
              u.loader({
                "type" : "script",
                "src" : u.data.base_url_get_ttdid,
                "cb" : null,
                "loc" : "script",
                "id" : "utag_##UTID##_get_ttdid",
                "attrs" : {}
              });
              u.data.interval++;
            }
          }

          u.polling_interval = setInterval(function () {
            u.get_ttdid(u.data.interval, u.data.sin);
          }, u.data.delay);
        }

        utag.DB("send:##UTID##:COMPLETE");
      }
    };
    utag.o[loader].loader.LOAD(id);
  }("##UTID##", "##UTLOADERID##"));
} catch (error) {
  utag.DB(error);
}
//end tealium universal tag
